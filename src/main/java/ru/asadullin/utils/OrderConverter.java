package ru.asadullin.utils;


import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.asadullin.dto.OrderDto;
import ru.asadullin.dto.OrderStatusDto;
import ru.asadullin.entity.Order;
import ru.asadullin.enums.Status;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
@RequiredArgsConstructor
public class OrderConverter {
    private DateTimeFormatter formatter;

    @PostConstruct
    public void init() {
        this.formatter = DateTimeFormatter.ofPattern("HH:mm dd:MM:yyyy");

    }

    public Order convertOrderDtoToOrder(OrderDto orderDto) {
        return Order.builder()
                .recipient(orderDto.getFirstName())
                .addressFrom(orderDto.getAddressFrom())
                .addressTo(orderDto.getAddressTo())
                .totalPrice(BigDecimal.valueOf(orderDto.getTotalPrice().doubleValue()))
                .createDate(formatter.format(LocalDateTime.now()))
                .status(Status.PROCESSED)
                .build();
    }

    public OrderStatusDto convertToOrderStatusDto(Order order) {
        return OrderStatusDto.builder()
                .id(order.getId())
                .firstName(order.getRecipient())
                .addressFrom(order.getAddressFrom())
                .addressTo(order.getAddressTo())
                .totalPrice(order.getTotalPrice())
                .creationDateTime(order.getCreateDate())
                .status(order.getStatus())
                .build();
    }
}

package ru.asadullin.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public enum Status {

    PROCESSED("WAITING FOR DELIVERY"),
    DELIVERING("DELIVERING"),
    COMPLETED("DELIVERED");


    @Getter
    private final String name;

}
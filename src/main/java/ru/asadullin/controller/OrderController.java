package ru.asadullin.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.asadullin.dto.OrderStatusDto;
import ru.asadullin.entity.Order;
import ru.asadullin.enums.Status;
import ru.asadullin.exeption.ResourceNotFoundException;
import ru.asadullin.service.OrderService;

import java.util.List;


@Slf4j
@RequestMapping("/api/orders")
@RestController
@RequiredArgsConstructor
public class OrderController {
    private final OrderService orderService;

    @PutMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public OrderStatusDto changeOrderStatusToDelivered(@RequestBody Order order) {
        if (order.getId() == null || !orderService.existsById(order.getId())) {
            throw new ResourceNotFoundException("Order with id: " + order.getId() + " does not exist");
        }else if (order.getStatus().equals(Status.PROCESSED)) {
            return orderService.updateOrderStatusToDelivered(order);
        }
        return orderService.updateOrderStatusToCompleted(order);
    }


    @GetMapping("/processed")
    public List<OrderStatusDto> findAllProcessedOrders(){
        return orderService.findAllByStatusProcessed(Status.PROCESSED);
    }

    @GetMapping("/delivered")
    public List<OrderStatusDto> findAllDeliveredOrders(){
        return orderService.findAllByStatusProcessed(Status.DELIVERING);
    }

    @GetMapping("/completed")
    public List<OrderStatusDto> findAllCompletedOrders(){
        return orderService.findAllByStatusProcessed(Status.COMPLETED);
    }
}

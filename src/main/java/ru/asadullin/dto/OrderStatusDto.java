package ru.asadullin.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.asadullin.enums.Status;

import java.math.BigDecimal;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderStatusDto {
    private Long id;
    private String firstName;
    private String addressFrom;
    private String addressTo;
    private BigDecimal totalPrice;
    private String creationDateTime;
    private Status status;
}

FROM adoptopenjdk/openjdk11:alpine-jre
ARG JAR_FILE=target/OrderService-0.0.1-SNAPSHOT.jar
WORKDIR /opt/app
COPY ${JAR_FILE} OrderService.jar
ENTRYPOINT ["java","-jar","OrderService.jar"]




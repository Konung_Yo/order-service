package ru.asadullin.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.asadullin.enums.Status;


import java.math.BigDecimal;

@Entity
@Table(name = "orders")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order {

    @Id
    @Column(name = "id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "recipient", nullable = false)
    private String recipient;

    @Column(name = "address_from", nullable = false)
    private String addressFrom;

    @Column(name = "address_to", nullable = false)
    private String addressTo;

    @Column(name = "price", nullable = false)
    private BigDecimal totalPrice;

    @Column(name = "create_date", nullable = false)
    private String createDate;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private Status status;

}

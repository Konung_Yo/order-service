# Order Service

### Описание проекта

Order Service - приложение службы доставки. 
При получении DTO заказа через RabbitMQ от магазина конвертирует 
в свою сущность Order и сохраняет в БД и присваивает статус заказа.
При получении курьером заказа на доставку приложение отправляет через RabbitMQ смену статуса заказа в магазин,
и обновляет статус в БД.
Сохранение и получение данных производится в формате JSON.


### Стек технологий

```
IntelliJ IDEA Ultimate
Java SE Development 11
Apache Maven version 4.0.0
Git
Spring Boot
Spring WebMVC
Spring Data JPA
Flyway
RabbitMQ
Jackson
PostgreSQL
Lombok
Docker
```

### Разработчик

```
Асадуллин Эльнур Маратович
Asadelmar@gmail.com
```
package ru.asadullin.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;
import ru.asadullin.dto.OrderDto;
import ru.asadullin.dto.OrderStatusDto;
import ru.asadullin.entity.Order;
import ru.asadullin.enums.Status;
import ru.asadullin.repository.OrderRepository;
import ru.asadullin.utils.OrderConverter;

import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;


@Slf4j
@Service
@RequiredArgsConstructor
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderConverter converter;
    private final ObjectMapper mapper;
    private OrderDto orderDto;
    private final AmqpTemplate amqpTemplate;

    @PostConstruct
    public void init() {
        this.orderDto = null;
    }


    @RabbitListener(queues = "delivery.queue")
    public void orderListener(Message message) {
        try {
            String orderMessage = new String(message.getBody(), StandardCharsets.UTF_8);
            orderDto = mapper.readValue(orderMessage, OrderDto.class);
            log.info("New delivery order received: " + orderDto.toString());
        } catch (Throwable th) {
            log.error("Error when receiving order message", th);
        }
        Order order = converter.convertOrderDtoToOrder(orderDto);
        saveOrUpdate(order);
    }

    public List<OrderStatusDto> findAllByStatusProcessed(Status ds) {
        return orderRepository.findAllByStatus(ds).stream().map(converter::convertToOrderStatusDto).collect(Collectors.toList());
    }

    public OrderStatusDto saveOrUpdate(Order order) {
        return converter.convertToOrderStatusDto(orderRepository.save(order));
    }

    public OrderStatusDto updateOrderStatusToDelivered(Order order) {
        order.setStatus(Status.DELIVERING);
        OrderStatusDto orderStatusDto = converter.convertToOrderStatusDto(orderRepository.save(order));
        amqpTemplate.convertAndSend("onlineshop.exchange", "delivery.status", "Delivering");
        return orderStatusDto;
    }

    public OrderStatusDto updateOrderStatusToCompleted(Order order) {
        order.setStatus(Status.COMPLETED);
        return converter.convertToOrderStatusDto(orderRepository.save(order));
    }

    public Optional<Order> findById(Long id) {
        return orderRepository.findById(id);
    }

    public boolean existsById(Long id) {
        return orderRepository.existsById(id);
    }

}

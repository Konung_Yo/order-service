
drop table if exists orders;
create table orders (
   id                    bigserial primary key,
   recipient             VARCHAR(30) NOT NULL UNIQUE,
   address_from          VARCHAR(255) NOT NULL,
   address_to            VARCHAR(255) NOT NULL,
   price                 numeric(8,2) NOT NULL,
   create_date           VARCHAR(255) NOT NULL,
   status                VARCHAR(255) NOT NULL
);


insert into orders(recipient,address_from,address_to, price,create_date,status)
values
('Иван','Гоголя 10','Ковшовой 20', 1500.0, '15:01 03:04:2022','PROCESSED'),
('Алексей','Пушкина 5','Колотушкина 10', 2000.0, '15:00 03:03:2022','PROCESSED');


